(function () {
  'use strict';

  /**
   * Initialize Bubble Chart
   * @param id {String}
   * @param options {Object}
   */
  VizabiPrototype.prototype.BubbleChart = function () {
    var options = this.options;
    var htmlID = this.htmlID;
    var dataset = this.dataset;

    //Sets the textArea with the current option/state
    document.getElementById("stateText").value = JSON.stringify(options, null, ' ');

    //State Options
    var opacityRegular = options.state.entities.opacityRegular;
    var opacityDim = options.state.entities.opacitySelectDim;
    var x_axisScaleType = options.state.marker.axis_x.scaleType;
    var y_axisScaleType = options.state.marker.axis_y.scaleType;
    var radius_ScaleType = options.state.marker.size.scaleType;
    var xAxis_which = options.state.marker.axis_x.which;
    var yAxis_which = options.state.marker.axis_y.which;
    var radius_which = options.state.marker.size.which;
    var x_Axis_unit = options.state.marker.axis_x.unit;
    var y_Axis_unit = options.state.marker.axis_y.unit;
    var startTime = options.state.time.start;
    var endTime = options.state.time.end;
    var valueYear = options.state.time.value;
    var trailsBool = options.state.time.trails;

    //Filter only relevant correct data.
    dataset = dataset[0].filter(function (d) {
      return (d.time && d['geo.cat'][0] === 'country' && d.pop && d.lex && d.gdp_per_cap);
    });

    //Returns the correct object based on string value in the state (when you change which object should be in which axis.)
    function whichSelector(value, d){
      if(value == 'lex')
        return d.lex;
      else if(value == 'gdp_per_cap')
        return d.gdp_per_cap;
      else if(value == 'pop')
        return d.pop;
    }

    d3.chart('BubbleChart', {

      initialize: function () {

        var chart = this;

        function scaleType(value){
          if(value == 'log')
            return d3.scale.log();
          else if(value == 'linear')
            return d3.scale.linear();
          else if(value == 'ordinal')
            return d3.scale.ordinal();
        }

        //resets the clicked items upon re-render
        if(options.state.entities.select.length > 0) options.state.entities.select.length = 0;
        if(d3.select("g.vp-select-lines")) d3.selectAll("g.vp-select-lines").remove();
        if(d3.select("div.vp-label")) d3.selectAll("div.vp-label").remove();

        //trail button
        d3.select('#trail-layer')
          .classed('vzb-invisible', false)
          .on('click',function(){
           trailsBool = !trailsBool;
            trailActivator();
            selectedDiv();
          });

        //Shows/Hides the trail layer
        function trailActivator() {
          var trlBtn = d3.select('#trail-layer');
          if(trailsBool == true)
            trlBtn.classed("btn-success", true);
          else
            trlBtn.classed('btn-success', false);
          if (options.state.entities.select.length > 0) {
            var trail = d3.select("g.vzb-bc-trails");
            if (trailsBool == true) {
              trail.classed("vzb-invisible", false);
            }
            else {
              trail.classed("vzb-invisible", true);
            }
          }
        }

        //Checks to see if that circle has been clicked.
        function hasValue(array, clicked){
          for (var i = 0; i < array.length; i++) {
            if (array[i].geo == clicked.geo)
              return true;
          }
          return false;
        }

        chart.xScale = scaleType(x_axisScaleType);
        chart.yScale = scaleType(y_axisScaleType);
        chart.pScale = scaleType(radius_ScaleType);

        chart.base.classed('Bubblechart', true);

        chart.layers = {};

        this.MARGINS = {
          top: 50,
          right: 20,
          bottom: 50,
          left: 70,
          divPadding: 10
        };

        //Backhand Year being displayed.
        chart.layers.backText = chart.base.append("g")
          .classed("vp-backText", true)
          .style("z-index", "0.8");

        chart.layers.backText.append("text")
          .attr('fill', '#CCCCCC')
          .style('opacity', 0.35);

        //Used to append X-axis within this layer.
        chart.layers.xlabels = chart.base.append("g")
          .attr("class", "vp-line-xaxis axis")
          .attr('fill', '#999999');

        //A layer for appending the x-axis labels being displayed (ex. GDP Per Capita /$year/person)
        chart.layers.xlabels_text = chart.base
          .append("g")
          .attr("class", "x_label")
          .append("text")
          .attr("text-anchor", "end");

        //Used to append y-axis within this layer.
        chart.layers.ylabels = chart.base.append("g")
          .attr("class", "vp-line-yaxis axis")
          .attr('fill', '#999999');

        //A layer for appending the y-axis labels being displayed.
        chart.layers.ylabels_text = chart.base
          .append("g")
          .attr("class", "y_label")
          .append("text");

        //A separate layers for trail circles to reside in.
        chart.layers.trails = chart.base.append("g")
          .classed("vzb-bc-trails", true);

        //Creates empty g classes with respective country names to be used for trailing
        for(var iter = 0; iter < options.state.entities.show.filter.geo.length; iter++)
        {
          chart.layers.trails.append("g").classed("vzb-bc-entity " + options.state.entities.show.filter.geo[iter],true);
        }

        //A layer for the actual circles to be rendered with respect to their data
        chart.layers.circles = chart.base.append('g')
          .classed('vp-circles', true);

        //A general layer for invisibleLineX and Y to reside in.
        chart.layers.dashStaticLine = chart.base.append('g')
          .classed('vp-dashed-static-line', true);

        //Dashed line shown on mouse hover
        chart.layers.invisibleLineX = chart.layers.dashStaticLine.append("line")
          .classed('vp-dash-line vp-invi-line-x', true)
          .style("display", "none");

        chart.layers.invisibleLineY = chart.layers.dashStaticLine.append("line")
          .classed('vp-dash-line vp-invi-line-y', true)
          .style("display", "none");

        //Mouse over tooltip
        chart.layers.tooltip = d3.select("#vp-bar-stage")
          .append("div")
          .classed('vp-tooltip-name', true)
          .style("position", "fixed")
          .style("padding", "0.2% 0.4% 0.2% 0.4%")
          .style("background-color", "#666")
          .style("color", "#F0F0F0")
          .style("z-index", "10")
          .style("visibility", "hidden")
          .text("");

        //Layer for floating div rendered over clicked circle
        chart.layers.selectedLabels = d3.select("#vp-bar-stage")
          .classed('labelClass', true);

        //Layer for line shown from clicked circle to floating div.
        chart.layers.selectedLines = chart.base.append('g')
          .classed('vp-select-lines', true);

        //Value displayed on axis upon mouse over.
        chart.layers.yAxisTooltip = chart.base.append("g")
          .classed("vp-tooltip", true)
          .style("visibility", "hidden")
          .style("z-index", "10");

        chart.layers.yAxisTooltip.append('rect')
          .attr("width", 20)
          .attr("height", 80)
          .attr('fill', '#FFFFFF')
          .style('opacity', 0.93);

        chart.layers.yAxisTooltip.append("text")
          .attr('fill', '#999999')
          .attr('font-size', 16)
          .attr("dy", ".30em");

        chart.layers.xAxisTooltip = chart.base.append("g")
          .classed("vp-tooltip", true)
          .style("visibility", "hidden")
          .style("z-index", "10");

        chart.layers.xAxisTooltip.append('rect')
          .attr("width", 100)
          .attr("height", 20)
          .attr('fill', '#FFFFFF')
          .style('opacity', 0.93);

        chart.layers.xAxisTooltip.append("text")
          .attr('fill', '#999999')
          .attr('font-size', 14.5)
          .attr("dy", ".30em");

        //Appends 'ALL' the possible circles of a particular country to create a trail.
        var trailGenerator = function(){
          if(options.state.entities.select.length > 0)
          {
            var lastIndex = options.state.entities.select.length - 1; //Only creates trail of element clicked.
            var counter = 0;
            for(var z = 0; z < dataset.length; z++)
              if(dataset[z].geo == options.state.entities.select[lastIndex].geo)
              {
                chart.layers.trails.select('.' + options.state.entities.select[lastIndex].geo).append('g').classed('trail-segment vzb-invisible a' + counter++, true).append('circle')
                  .style("fill", function () {
                    if (dataset[z]['geo.region'] == "eur" || dataset[z]['geo.region'] == "asi" || dataset[z]['geo.region'] == "afr" || dataset[z]['geo.region'] == "ame") {
                      return options.state.marker.color.palette[dataset[z]['geo.region']];
                    }
                    else
                      return options.state.marker.color.palette._default;
                  })
                  .style('stroke', 'white')
                  .style('opacity', opacityRegular)
                  .attr("r", function () {
                    return Math.abs(chart.pScale(dataset[z].pop));
                  })
                  .attr("cx", function () {
                    return chart.xScale(parseInt(dataset[z].gdp_per_cap))
                  })
                  .attr("cy", function () {
                    return chart.yScale(dataset[z].lex)
                  })
                  .attr("year", function(){
                    return dataset[z].time
                  })
                  .on('mouseover', function(){

                    chart.layers.invisibleLineX.style('display', null);
                    chart.layers.invisibleLineY.style('display', null);

                    chart.layers.invisibleLineX
                      .attr("x1", parseFloat(d3.select(this).attr('cx')))
                      .attr("y1", parseFloat(d3.select(this).attr('cy')))
                      .attr("x2", chart.MARGINS.left)
                      .attr("y2", parseFloat(d3.select(this).attr('cy')));

                    chart.layers.invisibleLineY
                      .attr("x1", parseFloat(d3.select(this).attr('cx')))
                      .attr("y1", parseFloat(d3.select(this).attr('cy')))
                      .attr("x2", parseFloat(d3.select(this).attr('cx')))
                      .attr("y2", chart.HEIGHT - chart.MARGINS.bottom);

                    chart.layers.tooltip.style("visibility", "visible").text(d3.select(this).attr('year'));

                    chart.layers.yAxisTooltip.select("rect").attr("x", chart.MARGINS.left - 27).attr("y", parseFloat(d3.select(this).attr('cy')) - 30);
                    chart.layers.yAxisTooltip.select("text").attr("x", chart.MARGINS.left - 25).attr("y", parseFloat(d3.select(this).attr('cy')))
                      .text(Math.floor(chart.yScale.invert(parseFloat(d3.select(this).attr('cy')))));

                    chart.layers.xAxisTooltip.select("rect").attr("x", parseFloat(d3.select(this).attr('cx')) - 52).attr("y", chart.HEIGHT - chart.MARGINS.bottom + 7);
                    chart.layers.xAxisTooltip.select("text").attr("x", parseFloat(d3.select(this).attr('cx')) - 12).attr("y", chart.HEIGHT - chart.MARGINS.bottom + 17)
                      .text(Math.floor(chart.xScale.invert(parseInt(d3.select(this).attr('cx')))));

                    chart.layers.yAxisTooltip.style('visibility', "visible");
                    chart.layers.xAxisTooltip.style('visibility', "visible");
                  })
                  .on('mouseout', function(){
                    chart.layers.yAxisTooltip.style("visibility", 'hidden');
                    chart.layers.xAxisTooltip.style('visibility', "hidden");
                    chart.layers.invisibleLineX.style('display', 'none');
                    chart.layers.invisibleLineY.style('display', 'none');
                    chart.layers.tooltip.style("visibility", "hidden");
                  })
                  .on('mousemove', function(){
                    chart.layers.tooltip.style("top", (d3.event.pageY - 5) + "px").style("left", (d3.event.pageX + 15) + "px");
                  });
              }
          }
        };

        //Hides/Shows the relevant trail circles
        var trailRenderer = function(){
          if(options.state.entities.select.length > 0)
          {
            for(var iter = 0; iter < options.state.entities.select.length; iter++) {
              //IF the current Year is ahead of the startTrailTime of a country, show all trails till that point.
              if (parseInt(chart.currentYear) >= parseInt(options.state.entities.select[iter].startTrailTime)) {
                for(var x = (+options.state.entities.select[iter].startTrailTime - +startTime); x <= (+chart.currentYear - +startTime); x++)
                  chart.layers.trails.select('g.' + options.state.entities.select[iter].geo).select('g.a' + x ).classed('vzb-invisible', false);
              }
              //Hides all the trail circles that are AFTER the currentYear.
              for(var z = (parseInt(endTime) - parseInt(startTime)); z > (parseInt(chart.currentYear) - parseInt(startTime)) ; z--) {
                chart.layers.trails.select('g.' + options.state.entities.select[iter].geo).select('g.a' + z).classed('vzb-invisible', true);
              }
              if(trailsBool == true)
                options.state.entities.select[iter].startTrailTime = (parseInt(chart.currentYear) < parseInt(options.state.entities.select[iter].startTrailTime))? chart.currentYear : options.state.entities.select[iter].startTrailTime;
              else
              {
                //IF trails is False: Hides the old trail circles that are before currentYear
                options.state.entities.select[iter].startTrailTime = chart.currentYear;
                for(var q = 0 ; q < (+options.state.entities.select[iter].startTrailTime - +startTime); q++)
                  chart.layers.trails.select('g.' + options.state.entities.select[iter].geo).select('g.a' + q ).classed('vzb-invisible', true);
              }
            }
          }
        };

        //transform the floating div according to year.
        var selectedDiv = function(){
          for(var iter = 0; iter < options.state.entities.select.length; iter++){
            var circle = d3.select('g.vzb-bc-entity.' + options.state.entities.select[iter].geo).selectAll('circle').filter(function () {
              return d3.select(this).attr('year') == chart.currentYear;
            });
            var sDiv = d3.select('div.vp-label.' + options.state.entities.select[iter].geo);
            var sLine = d3.select('line.vp-selectedLine.' + options.state.entities.select[iter].geo);
            var innerText = sDiv[0][0].innerText.split(' ');
            if(trailsBool == true) {
              if (parseInt(chart.currentYear) == parseInt(options.state.entities.select[iter].startTrailTime)) {

                sDiv[0][0].innerText = "" + innerText[0] + " " + options.state.entities.select[iter].startTrailTime;
                sDiv.style('top', parseFloat(circle.attr('cy')) + parseFloat(circle.attr('r')) + chart.MARGINS.divPadding + "px")
                  .style('left', parseFloat(circle.attr('cx')) + parseFloat(circle.attr('r')) + chart.MARGINS.divPadding + "px");

                options.state.entities.select[iter].startTrailTime = circle.attr('year');

                sLine.attr('x1', parseFloat(circle.attr('cx')))
                  .attr('y1', parseFloat(circle.attr('cy')))
                  .attr('x2', parseFloat(circle.attr('cx')) + parseFloat(circle.attr('r')) + chart.MARGINS.divPadding)
                  .attr('y2', parseFloat(circle.attr('cy')) + parseFloat(circle.attr('r')));
              }
            }
            else
            {
              options.state.entities.select[iter].startTrailTime = chart.currentYear;
              sDiv[0][0].innerText = "" + innerText[0];
              sDiv.style('top', parseFloat(circle.attr('cy')) + parseFloat(circle.attr('r')) + chart.MARGINS.divPadding + "px")
                  .style('left', parseFloat(circle.attr('cx')) + parseFloat(circle.attr('r')) + chart.MARGINS.divPadding + "px");

              sLine.attr('x1', parseFloat(circle.attr('cx')))
                .attr('y1', parseFloat(circle.attr('cy')))
                .attr('x2', parseFloat(circle.attr('cx')) + parseFloat(circle.attr('r')) + chart.MARGINS.divPadding)
                .attr('y2', parseFloat(circle.attr('cy')) + parseFloat(circle.attr('r')));
            }
          }
        };

        var circleDataBind = function (data) {

          //updates the variable to keep track of which Year it is, to be used in other functions.
          chart.setCurrentYear(data[0].time);

          chart.layers.backText.select("text").text(data[0].time);

          return this.selectAll('circle').data(data);
        };

        var circleUpdate = function () {

          this
            .style("fill", function (d) {
              if (d['geo.region'] == "eur" || d['geo.region'] == "asi" || d['geo.region'] == "afr" || d['geo.region'] == "ame") {
                  return options.state.marker.color.palette[d['geo.region']];
              }
              else
                return options.state.marker.color.palette._default;
            })
            .style('stroke', 'white')
            .attr("r", function (d) {
              return Math.abs(chart.pScale(parseInt(whichSelector(radius_which, d))));
            })
            .attr("cx", function (d) {
              return chart.xScale(parseInt(whichSelector(xAxis_which, d)))
            })
            .attr("cy", function (d) {
              return chart.yScale(parseFloat(whichSelector(yAxis_which, d)))
            });

          this
            .on("mouseover", function (d) {
              chart.layers.invisibleLineX.style('display', null);
              chart.layers.invisibleLineY.style('display', null);

              chart.layers.invisibleLineX
                .attr("x1", parseFloat(d3.select(this).attr('cx')))
                .attr("y1", parseFloat(d3.select(this).attr('cy')))
                .attr("x2", chart.MARGINS.left)
                .attr("y2", parseFloat(d3.select(this).attr('cy')));

              chart.layers.invisibleLineY
                .attr("x1", parseFloat(d3.select(this).attr('cx')))
                .attr("y1", parseFloat(d3.select(this).attr('cy')))
                .attr("x2", parseFloat(d3.select(this).attr('cx')))
                .attr("y2", chart.HEIGHT - chart.MARGINS.bottom);

              var countryName = d['geo.name'];

              if(options.state.entities.select.length == 0)
              {
                chart.layers.circles.selectAll("circle")
                  .style("opacity", opacityDim)
                  .filter(function (d) {
                    return d['geo.name'] == countryName;
                  })
                  .transition().duration(300)
                  .style("opacity", opacityRegular);
              }
              else{
                d3.select(this).style("opacity", opacityRegular);
              }

              chart.layers.tooltip.style("visibility", "visible").text(countryName);

              chart.layers.yAxisTooltip.select("rect").attr("x", chart.MARGINS.left - 27).attr("y", parseFloat(d3.select(this).attr('cy')) - 30);
              chart.layers.yAxisTooltip.select("text").attr("x", chart.MARGINS.left - 27).attr("y", parseFloat(d3.select(this).attr('cy'))).text(Math.floor(chart.yScale.invert(parseInt(d3.select(this).attr('cy')))));

              chart.layers.xAxisTooltip.select("rect").attr("x", parseFloat(d3.select(this).attr('cx')) - 52).attr("y", chart.HEIGHT - chart.MARGINS.bottom + 7);
              chart.layers.xAxisTooltip.select("text").attr("x", parseFloat(d3.select(this).attr('cx')) - 22).attr("y", chart.HEIGHT - chart.MARGINS.bottom + 17).text(Math.floor(chart.xScale.invert(parseInt(d3.select(this).attr('cx')))));

              chart.layers.yAxisTooltip.style('visibility', "visible");
              chart.layers.xAxisTooltip.style('visibility', "visible");
            })
            .on("mouseout", function (d) {
              chart.layers.yAxisTooltip.style("visibility", 'hidden');
              chart.layers.xAxisTooltip.style('visibility', "hidden");
              chart.layers.invisibleLineX.style('display', 'none');
              chart.layers.invisibleLineY.style('display', 'none');

              if(options.state.entities.select.length == 0)
              {
                chart.layers.circles.selectAll("circle").style("opacity", opacityRegular);
              }
              else{
                if(hasValue(options.state.entities.select, d) == false)
                  d3.select(this).style("opacity", opacityDim);
              }

              chart.layers.tooltip.style("visibility", "hidden");
            })
            .on("mousemove", function () {
              chart.layers.tooltip.style("top", (d3.event.pageY - 5) + "px").style("left", (d3.event.pageX + 15) + "px");
            })
            .on("click", function(d){
                var selectObject = {};

                if(hasValue(options.state.entities.select, d) == false)
                {
                  selectObject = {
                    geo: d.geo,
                    startTrailTime: d.time
                  };
                  options.state.entities.select.push(selectObject);

                  chart.layers.selectedLabels.append('div')
                    .classed('vp-label ' + d.geo, true)
                    .style("position", "absolute")
                    .style("padding", "0.2% 0.4% 0.2% 0.4%")
                    .style("z-index", "10")
                    .style("visibility", "visible")
                    .text(" " + d['geo.name'] + " " + options.state.entities.select[options.state.entities.select.length -1].startTrailTime)
                    .style("top", (parseFloat(d3.select(this).attr('cy')) + parseFloat(d3.select(this).attr('r')) + chart.MARGINS.divPadding) + "px").style("left", (parseFloat(d3.select(this).attr('cx')) + parseFloat(d3.select(this).attr('r'))  + chart.MARGINS.divPadding) + "px");

                  chart.layers.selectedLines.append('line')
                    .classed('vp-selectedLine ' + d.geo, true)
                    .attr('x1', parseFloat(d3.select(this).attr('cx')))
                    .attr('y1', parseFloat(d3.select(this).attr('cy')))
                    .attr('x2', parseFloat(d3.select(this).attr('cx')) + parseFloat(d3.select(this).attr('r')) + chart.MARGINS.divPadding)
                    .attr('y2', parseFloat(d3.select(this).attr('cy')) + parseFloat(d3.select(this).attr('r')))
                    .style('stroke-width', '1')
                    .style('stroke', '#607889');

                  trailGenerator();
                }

                else if(hasValue(options.state.entities.select, d) == true)
                {
                  for (var i =0; i < options.state.entities.select.length; i++)
                    if (options.state.entities.select[i].geo === d.geo) {
                      options.state.entities.select.splice(i,1);
                      break;
                    }
                  chart.layers.trails.select('.' + d.geo).html('');

                  d3.selectAll('div.' + d.geo).remove();
                  d3.selectAll('line.' + d.geo).remove();
                }

                if(options.state.entities.select.length > 0)
                {
                  chart.layers.circles.selectAll("circle").style("opacity", opacityDim);
                  for(var z = 0; z < options.state.entities.select.length; z++){
                    chart.layers.circles.selectAll("circle")
                      .filter(function (d) {
                        return d.geo == options.state.entities.select[z].geo;
                      })
                      .style("opacity", opacityRegular);
                  }
                }

            });

          //Must run with every Update function called.
          trailRenderer();
          trailActivator();
          selectedDiv();
        };

        var circleRender = function () {
          // Add the circles
          return this
            .append("circle")
            .style('opacity', opacityRegular);
        };

        var circleEvents = {
          enter: circleUpdate,
          update: circleUpdate,
          exit: function () {
            this.remove();
          }
        };

        chart.layer('circle', chart.layers.circles, {
          dataBind: circleDataBind,
          insert: circleRender,
          events: circleEvents
        });
      },

      transform: function (data) {

        var minXAxis = d3.min(dataset, function (d) {
          return parseInt(whichSelector(xAxis_which, d));
        });

        var maxXAxis = d3.max(dataset, function (d) {
          return parseInt(whichSelector(xAxis_which, d));
        });

        var xRange = [this.MARGINS.left, this.WIDTH - this.MARGINS.right];

        chart.xScale.range(xRange).domain([minXAxis, maxXAxis]).base(2).nice();

        chart.layers.xlabels.attr(
          'transform',
          'translate(0,' + (chart.HEIGHT - chart.MARGINS.bottom) + ')'
        );

        chart.layers.xlabels_text
          .attr("x", this.WIDTH - this.MARGINS.right)
          .attr("y", chart.HEIGHT - chart.MARGINS.bottom - 7)
          .text("GDP per capita, " + x_Axis_unit);

        chart.layers.ylabels_text
          .attr("x", chart.MARGINS.left - 23)
          .attr("y", chart.MARGINS.top - 5)
          .text("Life expectancy, " + y_Axis_unit);

        var minYAxis = d3.min(dataset, function (d) {
          return (parseFloat(whichSelector(yAxis_which, d)));
        });

        var maxYAxis = d3.max(dataset, function (d) {
          return (parseFloat(whichSelector(yAxis_which, d)));
        });

        var yRange = [this.HEIGHT - this.MARGINS.top, this.MARGINS.bottom];

        chart.yScale.range(yRange).domain([minYAxis, maxYAxis]).nice();

        chart.layers.ylabels.attr(
          'transform',
          'translate(' + chart.MARGINS.left + ',0)'
        );

        var minp = d3.min(dataset, function (d) {
          return whichSelector(radius_which, d);
        });

        var maxp = d3.max(dataset, function (d) {
          return whichSelector(radius_which, d);
        });

        //Dynamically adjust the radius of the circles.
        //Todo: proper function which changes the radius, based on change in width.
        var pRange, rangeEnd;
        if(this.WIDTH < 350)
          rangeEnd = 7.5;
        else if(this.WIDTH < 580)
          rangeEnd = 7.65;
        else
          rangeEnd = 9.2;

        pRange = [5, rangeEnd];

        // Todo: create a function which limits the max radius of any circle to that provided in the State API.
        chart.pScale.range(pRange).domain([minp, maxp]);

        //Hides every alternate tick value
        function logFormat(d, i) {
          if (i % 2 == 0)
            return Math.round(d);
          else
            return "";
        }

        var xAxis = d3.svg.axis().scale(chart.xScale).tickSize(7);
        if (this.WIDTH < 560) {
            xAxis.tickFormat(logFormat);
        }
        else {
           xAxis
            .tickFormat(function (d) {
              return d;
            });
        }

        var yAxis = d3.svg.axis()
          .scale(chart.yScale)
          .orient("left").ticks(6).tickFormat(function (d) {
            return d;
          });

        chart.layers.xlabels.call(xAxis);
        chart.layers.ylabels.call(yAxis);

        chart.layers.backText.select("text").attr("dy", ((this.HEIGHT)) / 2).attr("dx", ((this.WIDTH - chart.MARGINS.left - chart.MARGINS.right * 3)) / 2).attr("font-size", (this.WIDTH) * 0.1 + (this.HEIGHT) * 0.1);
        //Positions the backText correctly with large screen sizes.
        if (this.WIDTH > 600)
          chart.layers.backText.select("text").attr("transform", "translate(" + this.WIDTH * (-0.1) + "," + 0 + ")");

        chart.data = data;

        return data;
      },

      // properties
      width: function (newWidth) {
        if (arguments.length === 0) {
          return this.WIDTH;
        }
        this.WIDTH = newWidth;
        this.base.attr('width', newWidth);
        return this;
      },

      height: function (newHeight) {
        if (arguments.length === 0) {
          return this.HEIGHT;
        }
        this.HEIGHT = newHeight;
        this.base.attr('height', newHeight);
        return this;
      },

      setCurrentYear: function(year){
        if (arguments.length === 0) {
          return this.currentYear;
        }
        this.currentYear = year;
        return this;
      }

    });

    var chart = d3.select(htmlID)
      .append("svg")
      .chart("BubbleChart")
      .width(this.screenSize.width)
      .height(this.screenSize.height);

    //Obtain only data relevant to the year set in slider
    var initialData = filterArrayValueBy(dataset, 'time', valueYear);
    chart.draw(initialData);

    function filterArrayValueBy(data, key, value) {
      return data.filter(function (d) {
        return d[key] == value;
      });
    }

    var lastTime = null;

    this.Slider({
      data: dataset,
      transform: function (d, data) {
        d = Math.round(d);

        if (lastTime != d) {
          var dsc = filterArrayValueBy(data, 'time', d);
          if (dsc.length) {
            chart.draw(dsc);
          }
          lastTime = d;
        }
      }
    });
  };
})();
