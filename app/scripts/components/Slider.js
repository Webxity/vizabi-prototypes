(function() {
  'use strict';

  /**
   * Chroniton ( component used for slider )
   */
  VizabiPrototype.prototype.Slider = function(sOptions) {
    var sliderID = this.sliderID;
    var dataset = this.dataset;

    if (sOptions.data) {
      dataset = sOptions.data;
    }

    var currentChart = this.currentChart;
    var options = this.options;

    var optionLoop = options.state.time.loop;
    var optionValue = options.state.time.value;
    var optionPlayable = options.state.time.playable;

    var styleProps = 'float:left;' +
      'margin-top: 20px;' +
      'padding: 11px 12px;' +
      'border: 1px solid;' +
      'border-radius: 25px;' +
      'font-size: 16px;' +
      'text-align: center;' +
      'padding-left: 17px;'+
      'background-color: #607889;'+
      'width: 45px;' +
      'height: 45px;';

    var button = d3.select(sliderID)
      .append('button')
      .attr('style', styleProps)
      .classed('fa fa-play text-success', true);

    if(optionPlayable == false)
      d3.select('div#slider button').classed('vzb-invisible', true);
    else
      d3.select('div#slider button').classed('vzb-invisible', false);

    var playPause = function() {
      if (c.playing()) {
        button.classed('fa-play', false);
        button.classed('fa-pause', true);
      } else {
        button.classed('fa-play', true);
        button.classed('fa-pause', false);
      }
    };

    button.on('click', function () {
      c.playPause();
      playPause();
    });

    var c = chroniton()
      .domain([options.state.time.start, options.state.time.end])
      .setInitialValue(optionValue)
      .width(this.screenSize.width).loop(optionLoop);

    d3.select(sliderID)
      .append('span')
      .attr('style', 'float:left;width:80%;margin-top:15px;')
      .attr('class', 'theme-example')
      .call(c);

    c.on('change', function (d) {
      if (typeof sOptions.transform === 'function') {
        sOptions.transform.call(currentChart, d, dataset);
      }
      playPause();
    });
    c.playbackRate(1);

    if(this.screenSize.width < 350)
      c.playbackRate(1);
    else if(this.screenSize.width < 580)
      c.playbackRate(1.5);
    else if(this.screenSize.width < 900)
      c.playbackRate(2);
    else
      c.playbackRate(2);

    c.hideLabel();

    return c;
  };
})();
