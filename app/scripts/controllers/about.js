'use strict';

/**
 * @ngdoc function
 * @name vizabiPrototypesApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the vizabiPrototypesApp
 */
angular.module('vizabiPrototypesApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
